publish:
	cargo publish --package rive-models
	cargo publish --package rive-http
	cargo publish --package rive-gateway
	cargo publish --package rive-autumn
	cargo publish --package rive-cache-inmemory
	cargo publish --package rive
